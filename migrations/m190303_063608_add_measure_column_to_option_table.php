<?php

use yii\db\Migration;

/**
 * Handles adding measure to table `{{%option}}`.
 */
class m190303_063608_add_measure_column_to_option_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('option', 'measure', $this->string()->comment('Мера значения параметра'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('option', 'measure');
    }
}
