<?php

use yii\db\Migration;

/**
 * Class m190210_065937_change_type_node_name_column_to_journal_table
 */
class m190210_065937_change_type_node_name_column_to_journal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('journal', 'node_name');
        $this->addColumn('journal', 'node_id', $this->integer()->comment('Код узла'));
        $this->createIndex('in_journal$node_id', 'journal', 'node_id');
        $this->addForeignKey(
            'fk_node__id____journal__node_id',
            'journal',
            'node_id',
            'node',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190210_065937_change_type_node_name_column_to_journal_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190210_065937_change_type_node_name_column_to_journal_table cannot be reverted.\n";

        return false;
    }
    */
}
