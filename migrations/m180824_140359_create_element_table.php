<?php

use yii\db\Migration;

/**
 * Handles the creation of table `element`.
 */
class m180824_140359_create_element_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('element', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'equipment_id' => $this->integer(),
            'element_number' => $this->integer(),
            'element_row' => $this->integer(),
            'number' => $this->integer(),
            'modification' => $this->string(),
            'date_ust' => $this->dateTime(),
            'date_dem' => $this->dateTime(),
            'dem_reason' => $this->string(),
            'comment' => $this->string(),
            'user_id' => $this->integer(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('element');
    }
}
