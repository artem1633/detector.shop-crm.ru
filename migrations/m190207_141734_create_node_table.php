<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%node}}`.
 */
class m190207_141734_create_node_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%node}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование узла')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%node}}');
    }
}
