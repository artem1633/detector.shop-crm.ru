<?php

use yii\db\Migration;

/**
 * Handles the creation of table `journal`.
 */
class m180824_100950_create_journal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('journal', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'equipment_id' => $this->integer(),
            'user_id' => $this->integer(),
            'date' => $this->dateTime(),
            'type_id' => $this->integer(),
            'description' => $this->string(),
            'media' => $this->string(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('journal');
    }
}
