<?php

use yii\db\Migration;

/**
 * Handles the creation of table `equipment_object`.
 */
class m180820_131859_create_equipment_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('equipment_object', [
            'id' => $this->primaryKey(),
            'equipment_id' => $this->integer(),
            'object_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('equipment_object');
    }
}
