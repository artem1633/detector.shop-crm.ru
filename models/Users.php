<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $role_id
 */
class Users extends ActiveRecord
{
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_OPERATOR = 2;
    const USER_ROLE_TEH_SPEC = 3;
    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'password', 'login'], 'required'],
            [['login'], 'email'],
            [['login'], 'unique'],
            [['role_id'], 'integer'],
            [['fio', 'login', 'password', 'new_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'role_id' => 'Должность',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = md5($this->password);
        }

        if ($this->new_password != null) {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_OPERATOR, 'name' => 'Оператор',],
            ['id' => self::USER_ROLE_TEH_SPEC, 'name' => 'Тех. специалист',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if ($this->role_id == self::USER_ROLE_ADMIN) return 'Администратор';
        if ($this->role_id == self::USER_ROLE_OPERATOR) return 'Оператор';
        if ($this->role_id == self::USER_ROLE_TEH_SPEC) return 'Тех. специалист';
        return null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['user_id' => 'id']);
    }

    /**
     * Если пользователь существует - возвращает его ID, есл пользователя нет добавляет его (только fio).
     * При неудачном добавлении возвращает массив с ошибками
     *
     * @param string $fio Фамилия и инциалы пользователя
     * @return array|int
     */
    public static function getUserIdForImport($fio)
    {
//        Yii::info('ФИО на входе: ' . $fio, 'test');

        if (!strpos($fio, '.')){
            //Если точки не найдены подставляем точки к буквам имени и отчества, т.к. в файле журнала без точек, а в файле параметров и в базе с точками
            $part_fio = explode(' ', $fio);
            mb_regex_encoding('UTF-8');
            mb_internal_encoding("UTF-8");
            $letters = preg_split('/(?<!^)(?!$)/u', $part_fio[1]);
            $fio = $part_fio[0] . " {$letters[0]}.{$letters[1]}.";

            Yii::info('ФИО на выходе: ' . $fio, 'test');
        }


        $model = Users::find()->where(['fio' => $fio])->one();



        if (isset($model->id)) return $model->id;

        //Добавляем пользователя
        $user_model = new User();
        $user_model->fio = $fio;
        if ($user_model->save(false)) { //Без валидации, т.к. в файле импорта нет данных о логине и пароле пользователя
            Yii::info('Добавлен пользователь: ' . $user_model->fio);
            return $user_model->id;
        } else {
            Yii::error($user_model->errors, 'error');
            return $user_model->errors;
        }
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'fio');
    }

    public static function isAdmin(){
        $role_id = self::findOne(Yii::$app->user->id)->role_id ?? null;

        if ($role_id && $role_id == self::USER_ROLE_ADMIN) return true;

        return false;

    }
}
