<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }

    public function upload($tmp_name)
    {
        Yii::info('this UploadForm', 'test');
        if ($this->validate()) {
            if (!file_exists('uploads')){
                mkdir('uploads');
            }
            $this->file->saveAs('uploads/' . $tmp_name . '.' . $this->file->extension);
            return true;
        } else {
            Yii::error($this->errors, 'error');
            return false;
        }
    }
}