<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class ProductObject extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_object';
    }

    /**
     * Проверяет наличие связи, если записи нет - создает связь Продукт-Объект(предприятие)
     * @param int $product_id Код продукта
     * @param int $object_id Код объекта (предприятия)
     * @return bool
     */
    public static function addRelation($product_id, $object_id)
    {
        $relation_id = self::find()->where(['product_id' => $product_id])->andWhere(['object_id' => $object_id])->one()->id ?? null;

        if (!$relation_id){
            //Если связь не найдена - добавляем
            $model = new ProductObject();

            $model->product_id = $product_id;
            $model->object_id = $object_id;

            if (!$model->save()){
                Yii::error($model->errors, 'error');
                return false;
            }
        }

        return true;

    }
}
