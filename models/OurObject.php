<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "object".
 *
 * @property int $id
 * @property string $name
 */
class OurObject extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название объекта',
        ];
    }

    public function getEquipments()
    {
        return $this->hasMany(Equipment::className(), ['id' => 'equipment_id'])->viaTable('equipment_object', ['object_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('equipment_object', ['object_id' => 'id']);
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = ArrayHelper::map(
                self::find()
                    ->orderBy('name')->all(),
                'id', 'name'
            );
        }

        return static::$list;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['object_id' => 'id']);
    }

    /**
     * Если предприятие найдено возвращает его ID, если не найдено создает его. При неудачном создании возвращает массив ошибок
     *
     * @param string $object_name Наименование предприятия
     * @return array|int|mixed
     */
    public static function getObjectIdForImport($object_name){
        $model = OurObject::find()->where(['name' => $object_name])->one();
        if (isset($model->id)){
            return $model->id;
        }

        //Если предприяте не найдено - добавляем его
        $object_model = new OurObject();
        $object_model->name = $object_name;
        if ($object_model->validate() && $object_model->save()){
            return $object_model->id;
        } else {
            Yii::error($object_model->errors, 'error');
            return $object_model->errors;
        }
    }
}
