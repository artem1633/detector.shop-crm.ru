<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "node".
 *
 * @property int $id
 * @property string $name Наименование узла
 */
class Node extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'node';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование узла',
        ];
    }

    public static function getList(){
        return ArrayHelper::map(Node::find()->asArray()->all(), 'id', 'name');
    }

    public static function getNodeIdForImport($node_name)
    {
        $node_id = Node::find()->where(['name' => $node_name])->one()->id;

        if ($node_id) return $node_id;

        //Если тип работ не найден - добавляем его
        $node_model = new Node();

        $node_model->name = $node_name;

        if ($node_model->save()){
            return $node_model->id;
        } else {
            Yii::error($node_model->errors, 'error');
            return $node_model->errors;
        }
    }
}
