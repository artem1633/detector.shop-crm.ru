<?php

namespace app\models;

use Yii;

class EquipmentObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment_object';
    }

    /**
     * Проверяет наличие связи, если записи нет - создает связь Оборудование-Объект(предприятие)
     * @param int $equipment_id Код оборудования
     * @param int $object_id Код объекта (предприятия)
     * @return bool
     */
    public static function addRelation($equipment_id, $object_id)
    {
        $relation_id = self::find()->where(['equipment_id' => $equipment_id])->andWhere(['object_id' => $object_id])->one()->id ?? null;

        Yii::info('Код связи оборудование-предприятие: ' . $relation_id, 'test');

        if (!$relation_id){
            //Если связь не найдена - добавляем
            $model = new ProductObject();

            $model->equipment_id = $equipment_id;
            $model->object_id = $object_id;

            if (!$model->save()){
                Yii::error($model->errors, 'error');
                return false;
            }
        }
        Yii::info('Все норм.', 'test');

        return true;
    }
}
