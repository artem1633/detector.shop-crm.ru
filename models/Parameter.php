<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "parameter".
 *
 * @property int $id
 * @property int $object_id
 * @property int $user_id
 * @property int $equipment_id
 * @property string $date
 * @property int $product_id
 * @property int $option_id
 * @property string $value
 * @property int $number
 *
 * @property Equipment $equipment
 * @property Object $object
 * @property Option $option
 * @property Product $product
 * @property Users $user
 */
class Parameter extends ActiveRecord
{
    public $params;
    //Значения для формы добавления параметра в документ
    public  $date_value;
    public  $time_value;
    public  $text_value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'user_id', 'equipment_id', 'option_id', 'number', 'product_id'], 'integer'],
            [['date', 'date_value', 'time_value', 'text_value'], 'safe'],
            [['value'], 'string', 'max' => 255],
            // [['object_id', 'equipment_id', 'option_id', 'value', 'product_id'], 'required'],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => OurObject::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['value', 'validateValue'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Предприятие',
            'user_id' => 'Пользователь',
            'equipment_id' => 'Оборудование',
            'date' => 'Дата',
            'product_id' => 'Продукт',
            'option_id' => 'Параметр',
            'value' => 'Значение',
            'number' => 'Номер',
            'params' => 'Параметры'
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date = date('Y-m-d H:i:s');
            if (!isset($this->user_id)){
                $this->user_id = Yii::$app->user->identity->id;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public function getValueList()
    {
        return ArrayHelper::map([
            ['id' => 'text', 'name' => 'Текст',],
            ['id' => 'number', 'name' => 'Число',],
            ['id' => 'time', 'name' => 'Время',],
            ['id' => 'date', 'name' => 'Дата',],
        ], 'id', 'name');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getNameOption($id)
    {
        $value = Option::find()->where(['id' => $id])->one();
        return $value->name;
    }

    /**
     * @return string
     */
    public function getValeDescription()
    {
        if ($this->value == 'text') return 'Текст';
        if ($this->value == 'number') return 'Число';
        if ($this->value == 'time') return 'Время';
        if ($this->value == 'date') return 'Дата';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(OurObject::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Option::className(), ['id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @param $object_id
     * @return array
     */
    public function getEquipmentList($object_id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $object_id])->all();
        $array = [];
        foreach ($datas as $data) {
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);
        $equipments = Equipment::find()->where(['id' => $array])->all();
        return ArrayHelper::map($equipments, 'id', 'name');
    }

    /**
     * @param $object_id
     * @return array
     */
    public function getProductList($object_id)
    {
        $datas = ProductObject::find()->where(['object_id' => $object_id])->all();
        $array = [];
        foreach ($datas as $data) {
            $array [] = $data->product_id;
        }
        $array = array_unique($array);
        $product = Product::find()->where(['id' => $array])->all();
        return ArrayHelper::map($product, 'id', 'name');
    }

    /**
     * @param $attribute
     */
    public function validateValue($attribute)
    {
        $option = Option::findOne($this->option_id);
        $type = '';
        if ($option->value_type_id == 1) {
            $type = 'string';
            //if($this->money < 0) $this->addError($attribute, 'Вводите положителное средства');
        }
        if ($option->value_type_id == 3) {
            $type = 'number';
            if (!is_numeric($this->value)) $this->addError($attribute, 'Вводите числовое значение');
        }

    }

//    public function beforeDelete()
//    {
//        if ($this->option_id == Option::TYPE_DATE){
//            Yii::$app->session->setFlash('error', 'Удаление параметра невозможно');
//            return false;
//        }
//        return parent::beforeDelete();
//    }
}
