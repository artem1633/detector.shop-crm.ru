<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string $name
 *
 */
class Type extends \yii\db\ActiveRecord
{
    private static $list = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(self::$list)) {
            self::$list = ArrayHelper::map(
                self::find()
                    ->orderBy('name')->all(),
                'id', 'name'
            );
        }
        return self::$list;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Если тип найден возвращает ID типа работ, если не найден - создает его,
     * если создает удачно - возвращает ID созданного тип, если не удачно - возвращает масиив с ошибками
     *
     * @param $type_name
     * @return array|int|mixed
     */
    public static function getTypeIdForImport($type_name)
    {
        $type_id = Type::find()->where(['name' => $type_name])->one()->id;

        if ($type_id) return $type_id;

        //Если тип работ не найден - добавляем его
        $type_model = new Type();

        $type_model->name = $type_name;

        if ($type_model->save()){
            return $type_model->id;
        } else {
            Yii::error($type_model->errors, 'error');
            return $type_model->errors;
        }
    }
}
