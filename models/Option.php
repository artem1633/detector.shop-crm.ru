<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "option".
 *
 * @property int $id
 * @property string $name
 * @property int value_type_id
 * @property string $valueTypeLabel
 * @property string $measure Мера параметра
 */
class Option extends ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_FLOAT = 3;
    const TYPE_TIME = 4;
    const TYPE_DATE = 5;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'option';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['measure', 'name',], 'string', 'max' => 255],
            [['name', 'value_type_id',], 'required'],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Параметр',
            'value_type_id' => 'Тип значения',
            'valueTypeLabel' => 'Тип значения',
            'measure' => 'Мера значения',
        ];
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = [
                static::TYPE_STRING => 'Текст',
                static::TYPE_FLOAT => 'Число (дробное)',
                static::TYPE_TIME => 'Время',
                static::TYPE_DATE => 'Дата'
            ];
        }
        return static::$list;
    }


    /**
     * @return array
     */
    public function getOptionList()
    {
        $option = Option::find()->all();
        return ArrayHelper::map($option, 'id', 'name');
    }

    /**
     * @return mixed|null
     */
    public function getValueTypeLabel()
    {
        return self::getList()[$this->value_type_id] ?? null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['option_id' => 'id']);
    }

    public static function getName($id)
    {
        return self::findOne($id)->name;
    }

    /**
     * Получает единицу измерения параметра
     *
     * @param int $id ID option
     * @return string
     */
    public static function getMeasure($id)
    {
        return self::findOne($id)->measure;
    }

    /**
     * Получает еще не использованные id и наименования параметров.
     * @param int $number Значение поля number в таблице parameter
     * @return array|null
     */
    public static function getUnusedOptions($number)
    {
        $used_options = Parameter::find()->where(['number' => $number])->all();
        $options = Option::find()->all();

        if (count($used_options) == count($options)) return null;

        //Находим параметр, который еще не используется в документе
        $unused_options = [];
        foreach ($options as $option) {
            $current_unused_option = 0;
            foreach ($used_options as $used_option) {
                if ($used_option->option_id == $option->id) {
                    $current_unused_option = 0;
                    break 1;
                } else {
                    $current_unused_option = $option->id;
                }
            }
            if ($current_unused_option) {
//                Yii::info('Завершение итерации. Добавление в массив. Код параметра: ' . $current_unused_option, 'test');
                $unused_options[$option->id] = $option->name;
            }
        }
        Yii::info('Не используемые параметры: ' . $unused_options, 'test');
        Yii::info($unused_options, 'test');

        return $unused_options;
    }

    /**
     * Преобразуем дату из d/m/Y в Y-m-d
     *
     * @param int $id Код типа параметра (option)
     * @param string $value Значение у которого тип параметра == id
     * @return string
     */
    public static function getSavedValue($id, $value)
    {
        $option_model = self::findOne($id);

        if ($option_model->value_type_id == self::TYPE_DATE){
            $part_date = explode('/', $value);
            $formated_date = $part_date[2] . '-' . $part_date[1] . '-' . $part_date[0];

            Yii::info('Преобразованная дата: ' . $formated_date, 'test');

            return $formated_date;
        } else {
            return $value;
        }
    }


}
