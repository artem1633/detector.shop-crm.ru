<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property string $name
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $object_list;

    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Позиция оборудования',
            'objects' => 'Объект',
            'object_list' => 'Объекты',
        ];
    }

    public function getObjects()
    {
        return $this->hasMany(OurObject::className(), ['id' => 'object_id'])->viaTable('equipment_object', ['equipment_id' => 'id']);
    }

    public function getObjectsList()
    {
        $objects = OurObject::find()->all();

        return ArrayHelper::map($objects, 'id', 'name');
    }

    public function equipmentsObjectList()
    {
        $equipments = EquipmentObject::find()->where(['equipment_id' => $this->id])->all();

        $result = [];

        foreach ($equipments as $value) {
            $result [] = $value->object_id;
        }

        return $result;
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = ArrayHelper::map(
                self::find()
                    ->orderBy('name')->all(),
                'id', 'name'
            );
        }

        return static::$list;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['equipment_id' => 'id']);
    }

    /**
     * Если оборудвание найдено возвращает его ID, если не найдено создает его,
     * при удачном создании возвращает ID при неудачном возвращает массив с ошибками
     *Также создает связь в с оборудования с предприятем
     *
     * @param $equipment_name
     * @param int $object_id ID объекта
     * @return array|int|mixed
     */
    public static function getEquipmentIdForImport($equipment_name, $object_id)
    {
        $model = Equipment::find()->where(['name' => $equipment_name])->one();
        $equipment_id = $model->id ?? null;

        if ($equipment_id){
            self::connectObject($equipment_id, $object_id);
            return $equipment_id;
        }

        //Если оборудование не найдено - добавляем его
        $equipment_model = new Equipment();

        $equipment_model->name = $equipment_name;

        if ($equipment_model->save()) {
            self::connectObject($equipment_id, $object_id);
            return $equipment_model->id;
        } else {
            Yii::error($equipment_model->errors, 'error');
            return $equipment_model->errors;
        }
    }

    /**
     * Добавляет запись в EquipmentObject
     *
     * @param int $equipment_id Код оборудования
     * @param int $object_id Код предприятия
     * @return bool
     */
    private static function connectObject($equipment_id, $object_id)
    {
        //Создаем связь с предприятием
        if (!EquipmentObject::find()->where(['equipment_id' => $equipment_id])->andWhere(['object_id' => $object_id])->one()->id){
            $connect = new EquipmentObject();
            $connect->equipment_id = $equipment_id;
            $connect->object_id = $object_id;
            if (!$connect->save()) {
                Yii::error($connect->errors, 'error');
                return false;
            }
        }
        return true;
    }
}
