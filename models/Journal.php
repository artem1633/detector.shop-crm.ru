<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "journal".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $equipment_id
 * @property integer $user_id
 * @property string $date
 * @property integer $type_id
 * @property string $description
 * @property string $media
 *
 * @property OurObject $object
 * @property string $objectName
 * @property Equipment $equipment
 * @property string $equipmentName
 * @property Users $user
 * @property string $userName
 * @property Type $type
 * @property string $typeName
 * @property Node $node
 * @property int $node_id Код узла

 */
class Journal extends ActiveRecord
{
    public $date_start;
    public $date_end;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_id', 'object_id', 'equipment_id', 'user_id', 'type_id'], 'integer'],
            [['date'], 'safe'],
            [['description', 'media'], 'string', 'max' => 255],
            [['object_id', 'equipment_id', 'date', 'type_id', 'description'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Предприятие',
            'equipment_id' => 'Оборудование',
            'user_id' => 'Пользователь',
            'date' => 'Дата',
            'type_id' => 'Тип работ',
            'description' => 'Описание',
            'media' => 'Ссылка',
            'node_id' => 'Наименование узла',
            'objectName' => 'Предприятие',
            'equipmentName' => 'Оборудование',
            'userName' => 'Пользователь',
            'typeName' => 'Тип работ',
        ];
    }

    public function getObjectName()
    {
        return $this->object->name ?? null;
    }

    public function getEquipmentName()
    {
        return $this->equipment->name ?? null;
    }

    public function getUserName()
    {
        return $this->user->name ?? null;
    }

    public function getObject()
    {
        return $this->hasOne(OurObject::class, ['id' => 'object_id']);
    }

    public function getType()
    {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getTypeName()
    {
        return $this->type->name ?? null;
    }

    public function getEquipment()
    {
        return $this->hasOne(Equipment::class, ['id' => 'equipment_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    public function getNode()
    {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    public static function getUsersList()
    {
        $user = Users::find()->all();
        return ArrayHelper::map($user, 'id', 'fio');
    }

    public function getEquipmentList($object_id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $object_id ])->all();
        $array = [];
        foreach($datas as $data){
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);
        $equipments = Equipment::find()->where(['id' => $array])->all();
        return ArrayHelper::map($equipments, 'id', 'name');
    }

    /**
     * @param string $rus_date Дата в формате d.m.Y
     * @return null
     */
    public static function getDateForDb($rus_date){

        if (!strstr($rus_date, '.')) return null;

        $part_date = explode('.', $rus_date);

        $pendos_date = $part_date[2] . '-' . $part_date[1] . '-' . $part_date[0];

        \Yii::info($pendos_date, 'test');

        return $pendos_date;

    }
}
