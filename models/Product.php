<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $object_list;

    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название продукта',
            'objects' => 'Объект',
            'object_list' => 'Объекты',
        ];
    }

    public function getObjects()
    {
        return $this->hasMany(OurObject::className(), ['id' => 'object_id'])
            ->viaTable('equipment_object', ['product_id' => 'id']);
    }

    public function getObjectsList()
    {
        $objects = OurObject::find()->all();
        return ArrayHelper::map($objects, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getProductList()
    {
        $product = Product::find()->all();
        return ArrayHelper::map($product, 'id', 'name');
    }

    /**
     * @return array
     */
    public function productsObjectList()
    {
        $equipments = (new Query())
            ->from('product_object')
            ->where(['product_id' => $this->id])->all();

        $result = [];

        foreach ($equipments as $value) {
            $result [] = $value['object_id'];
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['product_id' => 'id']);
    }

    /**
     * Возвращает Код продукта по имени, если нет продукта сохдает его,
     * при ошибке возвращает массив с ошибками
     *
     * @param string $product_name Наименование продукта
     * @return array|int|mixed
     */
    public static function getProductIdForImport($product_name)
    {
        $product_id = self::find()->where(['name' => $product_name])->one()->id;

        if (!$product_id){
            //Добавляем пользователя
            $product_model = new Product();

            $product_model->name = $product_name;
            if ($product_model->save()){
                return $product_model->id;
            } else {
                Yii::error($product_model->errors, 'error');
                return $product_model->errors;
            }
        }
        return $product_id;
    }
}
