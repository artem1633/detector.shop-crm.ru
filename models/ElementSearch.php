<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Element;

/**
 * ElementSearch represents the model behind the search form about `app\models\Element`.
 */
class ElementSearch extends Element
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'equipment_id', 'element_number', 'element_row', 'number', 'user_id'], 'integer'],
            [['modification', 'date_ust', 'date_dem', 'dem_reason', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Element::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'object_id' => $this->object_id,
            'equipment_id' => $this->equipment_id,
            'element_number' => $this->element_number,
            'element_row' => $this->element_row,
            'number' => $this->number,
            'date_ust' => $this->date_ust,
            'date_dem' => $this->date_dem,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'modification', $this->modification])
            ->andFilterWhere(['like', 'dem_reason', $this->dem_reason])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
