<?php

namespace app\controllers;

use app\models\OurObject;
use app\models\Parameter;
use app\models\UploadForm;
use app\models\User;
use app\models\Users;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\ProductObject;
use app\models\Product;
use app\models\EquipmentObject;
use app\models\Equipment;
use app\models\Option;
use yii\web\UploadedFile;


/**
 * ParameterController implements the CRUD actions for Parameter model.
 */
class ParameterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionLists($id)
    {
        $datas = ProductObject::find()->where(['object_id' => $id])->all();
        foreach ($datas as $data) {
            $product = Product::findOne($data->product_id);
            echo "<option value = '" . $product->id . "'>" . $product->name . "</option>";
        }
    }

    public function actionListsEquipment($id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $id])->all();
        $array = [];
        foreach ($datas as $data) {
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);

        foreach ($array as $data) {
            $equipment = Equipment::findOne($data);
            echo "<option value = '" . $equipment->id . "'>" . $equipment->name . "</option>";
        }
    }

    public function actionOption($id)
    {
        $option = Option::findOne($id);
        if ($option->value_type_id == 1) return 'string';
        if ($option->value_type_id == 3) return 'number';

        return null;
    }

    /**
     * Lists all Parameter models.
     *
     * @return mixed
     */
    public function actionIndex()
    {

//        $request = Yii::$app->request;
//        $model = new Parameter();
        $dataProvider = new ActiveDataProvider([
            'query' => Parameter::find(),
            'pagination' => false,
        ]);

        /*        if ($request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    if ($request->isGet) {
                        return [
                            'title' => "Создать новый параметр",
                            'content' => $this->renderAjax('index', [
                                'model' => $model,
                                'dataProvider' => $dataProvider,
                            ]),
                            'footer' => Html::button('Close', [
                                    'class' => 'btn btn-default pull-left',
                                    'data-dismiss' => "modal",
                                ]) .
                                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"]),

                        ];
                    } else {
                        if ($model->load($request->post()) && $model->save()) {
                            return [
                                'forceReload' => '#crud-datatable-pjax',
                                'title' => "Создать новый параметр",
                                'content' => '<span class="text-success">Create Parameter success</span>',
                                'footer' => Html::button('Close', [
                                        'class' => 'btn btn-default pull-left',
                                        'data-dismiss' => "modal",
                                    ]) .
                                            Html::a('Create More', ['create'], [
                                                'class' => 'btn btn-primary',
                                                'role' => 'modal-remote',
                                            ]),

                            ];
                        } else {
                            return [
                                'title' => "Создать новый параметр",
                                'content' => $this->renderAjax('index', [
                                    'model' => $model,
                                    'dataProvider' => $dataProvider,
                                ]),
                                'footer' => Html::button('Close', [
                                        'class' => 'btn btn-default pull-left',
                                        'data-dismiss' => "modal",
                                    ]) .
                                            Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"]),

                            ];
                        }
                    }
                } else {
                    if ($model->load($request->post())) {
                        $model->save();
                        return $this->redirect(Yii::$app->request->referrer);
        //                return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        return $this->render('index', [
                            'model' => $model,
                            'dataProvider' => $dataProvider,
                        ]);
                    }
                }
        */


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Parameter model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    /*public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title' => "Parameter #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit', ['update', 'id' => $id], [
                                'class' => 'btn btn-primary',
                                'role' => 'modal-remote',
                            ]),
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
*/

    /**
     * Creates a new Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($id = null, $object_id = null, $equipment_id = null, $product_id = null)
    {

//        $request = Yii::$app->request;
        $model = new Parameter();
        $model->object_id = $object_id;
        $model->equipment_id = $equipment_id;
        $model->product_id = $product_id;

        if ($id > 0) {
            $query = Parameter::find()->where(['number' => $id]);
            $model->number = $id;
        } else $query = Parameter::find()->where(['id' => 'null']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'defaultOrder' => ['option_id' => SORT_ASC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],
        ]);

        if (!empty($_POST)) {

            $arr = $_POST;
            unset($arr['_csrf']);
            unset($arr['Parameter']);
            $count = 0;
            $numb = 0;
            foreach ($arr as $key => $value) {
                if (!empty($value)) {
                    $count++;
                    $a = explode('Paramaa-', $key);
                    $option_id = (int)$a[1];

                    $mode = new Parameter();
                    $mode->object_id = (int)$_POST['Parameter']['object_id'];
                    $mode->equipment_id = (int)$_POST['Parameter']['equipment_id'];
                    $mode->product_id = (int)$_POST['Parameter']['product_id'];
                    $mode->option_id = $option_id;
                    $mode->value = Option::getSavedValue($option_id, $value);
//                    $mode->value = $value;
                    $mode->save();

                    if ($count == 1) {
                        $mode->number = $mode->id;
                        $numb = $mode->number;
                        $mode->save();
                    } else {
                        $mode->number = $numb;
                        $mode->save();
                    }
                }


            }


            return $this->redirect(['create', 'id' => $numb, 'object_id' => (int)$_POST['Parameter']['object_id'], 'equipment_id' => (int)$_POST['Parameter']['equipment_id'], 'product_id' => (int)$_POST['Parameter']['product_id']]);
        } else {
            unset($_SESSION['numb']);
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Формирует отчет по заданным параметрам
     * @return string
     */
    public function actionReport()
    {
        $post = Yii::$app->request->post();


        if ($post['date_time_from'] != null || $post['date_time_from'] != "") $datefrom = date("Y-m-d 00:00:00", strtotime($post['date_time_from']));
        else {
            $datefrom = date('Y-m-d 00:00:00');
            $post['date_time_from'] = date('d.m.Y');
        }
        if ($post['date_time_to'] != null || $post['date_time_to'] != "") $dateto = date("Y-m-d 23:59:59", strtotime($post['date_time_to']));
        else {
            $dateto = date('Y-m-d 23:59:59');
            $post['date_time_to'] = date('d.m.Y');
        }


        $query = Parameter::find()
            ->where(['object_id' => $post['object']])
            ->andWhere(['option_id' => $post['parametr']])
            ->andWhere(['equipment_id' => $post['oborudovaniya']])
            ->andWhere(['between', 'date', $datefrom, $dateto])
            ->orderBy(['value' => SORT_ASC])
            ->orderBy(['option_id' => SORT_ASC]);


        //  $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination'=>['pageSize'=>5],
        // ]);

        $models = $query->all();

        return $this->render('report', [
            //'dataProvider'=>$dataProvider,
            'models' => $models,
            'post' => $post,
        ]);

    }

    /**
     * Формирует отчет по заданным параметрам
     * @return mixed
     */
    public function actionReportSecond()
    {
        $post = Yii::$app->request->post();


        if ($post['date_time_from'] != null || $post['date_time_from'] != "") {
            $datefrom = date("Y-m-d 00:00:00", strtotime($post['date_time_from']));
        } else {
            $datefrom = date('Y-m-d 00:00:00');
            $post['date_time_from'] = date('d.m.Y');
        }

        if ($post['date_time_to'] != null || $post['date_time_to'] != "") {
            $dateto = date("Y-m-d 23:59:59", strtotime($post['date_time_to']));
        } else {
            $dateto = date('Y-m-d 23:59:59');
            $post['date_time_to'] = date('d.m.Y');
        }

        if ($post['equipment']) {
            $equipment = $post['equipment'];
        }

        if ($post['object']) {
            $object = $post['object'];
        }

        if ($post['group_param']) {
            $group_param = $post['group_param'];
        }

        if ($post['unique_param']) {
            $unique_param = $post['unique_param'];
        }


//       Делаем выборку по дате, указанной в параметре "Дата"
        $sql = "SELECT DISTINCT(number) FROM parameter WHERE number IN (SELECT number FROM parameter WHERE option_id = 6 AND DATE(value) BETWEEN CAST(:datefrom AS DATE) AND CAST(:dateto AS DATE))";
        $numbers = Parameter::findBySql($sql, [':datefrom' => $datefrom, ':dateto' => $dateto])->asArray()->all();

        $query = Parameter::find();

        $query->andWhere(['IN', 'number', $numbers]);

        if (isset($equipment)) {
            $query->andWhere(['equipment_id' => $equipment]); //Выборка по оборудованию
        }

        if (isset($object)) {
            $query->andWhere(['object_id' => $object]); //Выборка по предприятию (объекту)
        }

        if (isset($unique_param)) {
            $unique_query = clone $query;
            $unique_values = $unique_query
                ->select(['value'])
                ->distinct()
                ->andWhere(['option_id' => $unique_param])
                ->orderBy(['CAST(value AS SIGNED)' => SORT_ASC])
                ->all();
        } else {
            $unique_values = '';
        }
        $param_query = clone $query;

        if (isset($group_param)) {
            $query->andWhere(['option_id' => $group_param]);
        }

        $models = $query->all();

        return $this->render('report-second', [
            //'dataProvider'=>$dataProvider,
            'models' => $models,
            'post' => $post,
            'unique_values' => $unique_values,
            'param_query' => $param_query,
        ]);

    }

    /**
     * Updates an existing Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load($request->post())){

            if ($model->option->value_type_id == Option::TYPE_DATE){
                //Преобразуем дату
                $part_date = explode('/', $model->value);
                $model->value = $part_date[2] . '-' . $part_date[1] . '-' . $part_date[0];
            }

            if ($model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Параметр",
                    'forceClose' => true,
                ];
            }
        }
        return [
            'title' => "Изменить",
            'content' => $this->renderAjax('update', [
                'model' => $model,
            ]),
            'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal",]) .
                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),
        ];

    }

    /**
     * Delete an existing Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $number = $model->number ?? null;

        //Удаление только тех строк в которых number != id, т.к. идет привязка по этим параметрам
        if ($model->id != $number) {
            $this->findModel($id)->delete();
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Ошибка удаления.',
                'content' => 'Удаление данного параметра невозможно!',
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]),
            ];
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['create?id=' . $number]);
        }
    }

    public function actionDeleteAll($number)
    {
        $request = Yii::$app->request;
        $models = Parameter::find()->where(['number' => $number])->all();
        foreach ($models as $value) {
            $value->delete();
        }
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose' => true, 'forceReload' => '#available-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Parameter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Parameter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parameter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Загрузка и импорт файла
     * @return array|mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionUpload()
    {
        if (!Users::isAdmin()){
            Yii::$app->session->setFlash('warning', 'Недостаточно прав для выполнения импорта.');
            $this->redirect('/journal');
        }
        $model = new UploadForm();
        $request = Yii::$app->request;
        $errors = [];

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Импорт параметров из файла",
//                'size' => 'medium',
                'content' => $this->renderAjax('import', [
                    'model' => $model,
                ]),
            ];
        } elseif (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $tmp_name = time() + rand(999, 99999999);

            //Поулчаем в каких ячейках лежат параметры: номер => Наименование параметра
            if (!$params = Option::find()->asArray()->all()) {
                Yii::error('Отсутствует список параметров! Импорт невозможен!', 'errors');
                Yii::$app->session->setFlash('error', 'Недоступен список параметров. Импорт невозможен!');
                return $this->actionIndex();
            }

            if ($model->upload($tmp_name)) {
                // Файл успешно загружен
                $inputFileName = 'uploads/' . $tmp_name . '.' . $model->file->extension;

                /** Load $inputFileName to a Spreadsheet Object  **/
                $spreadsheet = IOFactory::load($inputFileName);

                // объект Cells, имеющий доступ к содержимому ячеек
                $cells = $spreadsheet->getActiveSheet()->getCellCollection();
//                $active_sheet = $spreadsheet->getActiveSheet();
                $active_sheet = $spreadsheet->getSheetByName('Параметры');
                if (!$active_sheet) {
                    Yii::$app->session->setFlash('error', 'Импорт невозможен. В импортируем файле отсутствует лист "Параметры"');
                    return $this->redirect('index');
                }
                $columns_num = Coordinate::columnIndexFromString($cells->getHighestColumn());

                //Получаем наименования столбцов из первой строки листа
                $cols_headers = [];
                for ($cols_h = 1; $cols_h <= $columns_num; $cols_h++) {
                    $cols_headers[$cols_h] = $active_sheet->getCellByColumnAndRow($cols_h, 1)->getValue();
                }


                //Обект, оборудование, продукт,пользоваетль, дата и время могут не изменятся от строки к строке,
                //т.е. в файле продукт может быть указан только в первой строке, а в следующих быть пропущен, т.к. продукт тот же
                $object_id = 0;
                $equipment_id = 0;
                $output_id = 0;
                $user_id = 0;
                $date_param = 0;
                $time_param = 0;

                //Перебираем строки, начиная со второй
                for ($row = 2; $row <= $cells->getHighestRow(); $row++) {

//                    Yii::info('Строка номер: ' . $row, 'test');

                    //Перебираем ячейки в строке
                    /**
                     * @var int $current_num Номер, У всех параметров в одной строке файла
                     * один и тот же номер и равен ID первого параметра в строке
                     */
                    $current_num = 0;

                    for ($col = 1; $col <= $columns_num; $col++) {

                        //Получаем значение ячейки по адресу ($col,$row)
                        $cell_value = trim($active_sheet->getCellByColumnAndRow($col, $row)->getValue());

//                        Yii::info('Имя столбца: ' . trim($cols_headers[$col]), 'test');
//                        Yii::info('Значение ячейки: ' . $cell_value, 'test');

                        switch (trim($cols_headers[$col])) {
                            case 'Предприятие':
                                if ($cell_value) {
                                    //Если ячейка не пустая получаем ID если пустая - оставляем пердыдущий ID предприятия
                                    $object_id = OurObject::getObjectIdForImport($cell_value);
                                }
//                                Yii::info('Код предприятия: ' . $object_id, 'test');
                                break;
                            case 'Оборудование':
                                if ($cell_value) {
                                    $equipment_id = Equipment::getEquipmentIdForImport($cell_value, $object_id);
                                }
                                break;
                            case 'Пользователь':
                                if ($cell_value) {
                                    $user_id = User::getUserIdForImport($cell_value);
                                }
                                break;
                            case 'Продукт':
                                if ($cell_value) {
                                    $output_id = Product::getProductIdForImport($cell_value);
                                }
//                                Yii::info('Наименование продукта: ' . $cell_value, 'test');
                                break;
                            default:

                                break;
                        }
                    }

                    //Добавляем связь Продукт - Объект
                    if (!ProductObject::addRelation($output_id, $object_id)) array_push($errors, 'Ошбика сохранения связи Продукт-Объект');

                    //Добавляем связь Оборудование - Объект
                    if (!EquipmentObject::addRelation($equipment_id, $object_id)) array_push($errors, 'Ошбика сохранения связи Оборудовние-Объект');

                    for ($col = 1; $col <= $columns_num; $col++) {
                        //Получаем значение ячейки по адресу ($col,$row)
                        $cell_value = trim($active_sheet->getCellByColumnAndRow($col, $row)->getValue());

                        //Перебираем параметры
                        foreach ($params as $parameter) {
                            $param_name = $parameter['name'] ?? null;

                            if ($param_name == $cols_headers[$col] && $param_name != null) {
                                //Если имя параметра совпадает с заголовком колонки

                                //Проверяем тип параметра, если что преобразовываем
                                if (isset($parameter['value_type_id'])) {
                                    switch ($parameter['value_type_id']) {
                                        case Option::TYPE_DATE:
                                            if ($cell_value) {
                                                $date_p = ($cell_value - 25569) * 86400; //Переводим дату Excel в UNIX дату
//                                                Yii::warning($cell_value, 'test');
//                                                Yii::warning($date_p, 'test');
                                                $date_param = date('Y-m-d', $date_p);
                                            }
                                            $cell_value = $date_param;
//                                            Yii::warning($date_param, 'test');
                                            break;
                                        case Option::TYPE_TIME:
                                            if ($cell_value) {
                                                $time_param = NumberFormat::toFormattedString($cell_value, 'h:mm');
                                            }
                                            $cell_value = $time_param;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                $param_model = new Parameter();

                                $param_model->object_id = $object_id;
                                $param_model->user_id = $user_id;
                                $param_model->equipment_id = $equipment_id;
                                $param_model->date = date('Y-m-d H:i:s', time());
                                $param_model->product_id = $output_id;
                                $param_model->option_id = $parameter['id'] ?? null;
                                $param_model->value = $cell_value;
                                $param_model->number = $current_num;

                                if (!$param_model->save()) {
                                    Yii::warning('$option_id: ' . $param_model->option_id, 'test');
                                    Yii::warning('$cell_value: ' . $cell_value, 'test');
                                    Yii::error($param_model->errors, 'error');
                                } else {
                                    //Пишем номер в "number"
                                    if (!$current_num) {
                                        $param_model->number = $param_model->id;
                                        $current_num = $param_model->id;
                                    }
//                                    Yii::info('Сохраняем $param_model->number = ' . $param_model->number, 'test');
                                    $param_model->save();
                                }
                            } else {
                                continue;
                            }
                        }
                    }


                }
                unlink('uploads/' . $tmp_name . '.' . $model->file->extension);
            }
            if (count($errors)) {
                Yii::error('Все ошибки импорта', 'error');
                Yii::error($errors, 'error');
                $prepared_errors = '<pre>' . Json::encode($errors) . '</pre>';
                Yii::$app->session->setFlash('error', $prepared_errors);
            }
            return $this->redirect(['/parameter/index']);
        }
        return $this->redirect(['/parameter/index']);
    }

    /**
     * @return Response
     */
    public function actionSetParameter()
    {
        $model = new Parameter();
        $request = Yii::$app->request;

        if ($request->isPost && $model->load($request->post())) {
//            Yii::info($model->object_id, 'test');
            if (isset($model->number)) {
                $result = Parameter::updateAll([
                    'object_id' => $model->object_id,
                    'equipment_id' => $model->equipment_id,
                    'product_id' => $model->product_id,
                    'user_id' => $model->user_id,
                ], ['number' => $model->number]);
                Yii::info($result, 'test');
            }
        }
        return $this->redirect(['create', 'id' => $model->number]);
    }

    /**
     * @param $number
     * @return array|string|Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAddParameter($number)
    {
        $model = new Parameter();
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->number = $number;
            Yii::info('Код параметра: ' . $request->post('Parameter'), 'test');
            Yii::info($request->post('Parameter'), 'test');

            $parameters = $request->post('Parameter');

            if ($parameters) {
                //Получаем object_id, user_id и пр. по полю number
                $param_model = Parameter::find()->where(['number' => $number])->limit(1)->one();

                if (isset($param_model)) {
                    $model->object_id = $param_model->object_id;
                    $model->user_id = $param_model->user_id;
                    $model->equipment_id = $param_model->equipment_id;
                    $model->date = date('Y-m-d H:i:s', time());
                    $model->product_id = $param_model->product_id;
                    $model->option_id = $parameters['option_id'];
                }

                //Получаем значение 'value'
                if ($parameters['date_value']) {
                    $model->value = $parameters['date_value'];
                } elseif ($parameters['time_value']) {
                    $model->value = $parameters['time_value'];
                } else {
                    $model->value = $parameters['text_value'];
                }

                $model->save();
                return $this->redirect(['create', 'id' => $model->number]);
            } else {
                return [
                    'title' => "Добавление параметра #" . $number,
                    'size' => 'large',
                    'content' => $this->renderAjax('_param_form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                    Html::a('Сохранить', ['update-parameter'], [
//                        'class' => 'btn btn-primary',
////                        'role' => 'modal-remote',
//                    ]),
                        Html::button('Сохранить', [
                            'type' => 'submit',
                            'class' => 'btn btn-primary',
                            'role' => 'modal-remote',
                        ])
                ];
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return int|null
     */
    public function actionGetTypeValue($id)
    {
        return Option::findOne($id)->value_type_id ?? null;
    }
}