<?php

namespace app\controllers;

use app\models\Journal;
use app\models\JournalSearch;
use app\models\Node;
use app\models\OurObject;
use app\models\Type;
use app\models\UploadForm;
use app\models\Users;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\EquipmentObject;
use app\models\Equipment;
use yii\web\UploadedFile;

/**
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Journal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $searchModel = new JournalSearch();

        if ($request->isPost) {
            //Если передаются параметры фильтрации
            $dataProvider = new ActiveDataProvider([
                'query' => Journal::find(),
            ]);

            if ($request->post('date_start') && $request->post('date_end')) {
                $dataProvider->query->andWhere([
                    'BETWEEN',
                    'date',
                    Journal::getDateForDb($request->post('date_start')) . ' 00:00:00',
                    Journal::getDateForDb($request->post('date_end')) . ' 23:59:59'
                ]);
            }

            if ($request->post('object')) {
                $dataProvider->query->andWhere(['object_id' => $request->post('object')]);
            }

            if ($request->post('equipment')) {
                $dataProvider->query->andWhere(['equipment_id' => $request->post('equipment')]);
            }

            if ($request->post('type')) {
                $dataProvider->query->andWhere(['type_id' => $request->post('type')]);
            }

            $dataProvider->pagination= false;

        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionLists($id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $id])->all();
        $array = [];
        foreach ($datas as $data) {
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);

        foreach ($array as $data) {
            $equipment = Equipment::findOne($data);
            echo "<option value = '" . $equipment->id . "'>" . $equipment->name . "</option>";
        }
    }

    /**
     * Displays a single Journal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Журнал",
                'size' => 'normal',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Journal model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Journal();
        $model->user_id = Yii::$app->user->identity->id;

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Журнал",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Journal model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Журнал",
                    'size' => 'large',
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Journal model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Journal model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Journal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Journal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Загрузка и импорт файла
     * @return array|mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionUpload()
    {
        if (!Users::isAdmin()) {
            Yii::$app->session->setFlash('warning', 'Недостаточно прав для выполнения импорта.');
            $this->redirect('/journal');
        }
        $model = new UploadForm();
        $request = Yii::$app->request;
        $errors = [];
        $error_counter = 0;

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title' => "Импорт журнала из файла",
                'content' => $this->renderAjax('import', [
                    'model' => $model,
                ]),
            ];
        } elseif (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $tmp_name = time() + rand(999, 99999999);

            if ($model->upload($tmp_name)) {
                // Файл успешно загружен
                $inputFileName = 'uploads/' . $tmp_name . '.' . $model->file->extension;

                /** Load $inputFileName to a Spreadsheet Object  **/
                $spreadsheet = IOFactory::load($inputFileName);

                // объект Cells, имеющий доступ к содержимому ячеек
                $cells = $spreadsheet->getActiveSheet()->getCellCollection();
//                $active_sheet = $spreadsheet->getActiveSheet();
                $active_sheet = $spreadsheet->getSheetByName('Журнал работ');

//                Yii::info($active_sheet, 'test');

                if (!$active_sheet) {
                    Yii::$app->session->setFlash('error', 'Импорт невозможен. В импортируем файле отсутствует лист "Журнал"');
                    return $this->redirect('index');
                }

                for ($row = 2; $row <= $cells->getHighestRow(); $row++) {
                    $cols = [];
                    $error_counter++;
                    for ($col = 1; $col <= 7; $col++) {
                        $cols[] = $active_sheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

//                    Yii::info($cols, 'test');

                    if ($cols[0]) {
                        $journal_model = new Journal();

                        $date = ($cols[0] - 25569) * 86400; //Формула перевода даты Excel в UNIX дату
                        $user = $cols[1]; //Фамилия и инициалы пользователя
                        $object = $cols[2]; //Наименование предприятия
                        $equipment = $cols[3]; //Наименование оборудования
                        $type_work = $cols[4]; //Тип работ
                        $node_name = $cols[5]; //Наименование узла
                        $description = $cols[6]; //Описание

//                        Yii::info(date('d.m.Y', $date), 'test');

                        //Данные в модель
                        $journal_model->date = date('Y-m-d H:i:s', $date);

                        $user_id = Users::getUserIdForImport($user);

                        if (is_numeric($user_id)) { //Если вернулся ID пользователя
                            $journal_model->user_id = $user_id;
                        } else { //Если вернулись ошибки
                            $errors['Пользователь'][$error_counter] = $user_id;
                            continue;
                        }

                        $object_id = OurObject::getObjectIdForImport($object);

                        if (is_numeric($object_id)) {
                            $journal_model->object_id = $object_id;
                        } else {
                            $errors['Предприятие'][$error_counter] = $object_id;
                            continue;
                        }

                        $equipment_id = Equipment::getEquipmentIdForImport($equipment, $object_id);

                        if (is_numeric($equipment_id)) {
                            $journal_model->equipment_id = $equipment_id;
                        } else {
                            $errors['Оборудование'][$error_counter] = $equipment_id;
                            continue;
                        }

                        $type_id = Type::getTypeIdForImport($type_work);

                        if (is_numeric($type_id)) {
                            $journal_model->type_id = $type_id;
                        } else {
                            $errors['Тип работ'][$error_counter] = $type_id;
                            continue;
                        }

                        $node_id = Node::getNodeIdForImport($node_name);

                        if (is_numeric($node_id)) {
                            $journal_model->node_id = $node_id;
                        } else {
                            $errors['Узел'][$error_counter] = $node_id;
                            continue;
                        }

                        $journal_model->description = $description;

                        if (!$journal_model->save()) {
                            Yii::error($journal_model->errors);
                        };
                    }
                }
                //Удаляем на сервере импортированный файл
                unlink('uploads/' . $tmp_name . '.' . $model->file->extension);
            }
            if (count($errors)) {
                Yii::error($errors, 'error');
                Yii::$app->session->setFlash('warning', 'Имеются ошибки импорта');
            }
        }
        return $this->redirect(['/journal/index']);
    }
}
