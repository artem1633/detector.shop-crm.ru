<?php

/* @var $this yii\web\View */
/* @var $model app\models\Journal */
?>
<div class="journal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
