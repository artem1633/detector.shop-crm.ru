<?php

use app\models\Equipment;
use app\models\OurObject;
use app\models\Type;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$request = Yii::$app->request;

$layout = <<< HTML
<div class="input-group">
    <span id="start-addon" class="input-group-addon">С</span>
    {input1}
    <span id="end-addon" class="input-group-addon" >По</span>
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
</div>
HTML;
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'hidden-print',
        'action' => '/filter-journal'
    ],
]); ?>
<div class="row">
    <div class="col-xs-6">
        <label>Период</label><br>
        <?php
        try {
            echo DatePicker::widget([
                'type' => DatePicker::TYPE_RANGE,
                'name' => 'date_start',
                'value' => $request->post('date_start'),
                'name2' => 'date_end',
                'value2' => $request->post('date_end'),
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), __METHOD__);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        ?>
    </div>
    <div class="col-xs-6">
        <label>Предприятие</label><br>

        <?php
        try {
            echo Select2::widget([
                'name' => 'object',
                'language' => 'ru',
                'value' => $request->post('object'),
                'data' => ArrayHelper::map(OurObject::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите предприятия',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), __METHOD__);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <label>Оборудование</label><br>
        <?php
        try {
            echo Select2::widget([
                'name' => 'equipment',
                'language' => 'ru',
                'value' => $request->post('equipment'),
                'data' => Arrayhelper::map(Equipment::find()->all(), 'id', 'name'),
//                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => 'Выберите оборудование',
                    'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), __METHOD__);
            Yii::$app->session->setFlash('error', $e->getMessage());
        } ?>
    </div>
    <div class="col-xs-6">
        <label>Тип работ</label><br>
        <?php
        try {
            echo Select2::widget([
                'name' => 'type',
                'language' => 'ru',
                'value' => $request->post('type'),
                'data' => Arrayhelper::map(Type::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите оборудование',
                    'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), __METHOD__);
            Yii::$app->session->setFlash('error', $e->getMessage());
        } ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-4 pull-right">
        <div class="btn-group">
            <?= Html::a('Сбросить фильтр', Url::to('/journal/index'), ['class' => 'btn btn-warning']) ?>
            <?= Html::submitButton('Применить фильтр', ['class' => 'btn btn-info']) ?>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>