<?php

use app\models\User;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use moonland\phpexcel\Excel;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var array $post Массив параметров для полей фильтра */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProvider_export yii\data\ActiveDataProvider */
/* @var $searchModel app\models\JournalSearch */

$this->title = 'Журнал';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$import_btn = Html::a('Импорт из файла', ['/journal/upload'], [
    'role' => 'modal-remote',
    'title' => 'View',
    'data-toggle' => 'tooltip',
    'class' => 'btn btn-primary',
]);

if (!User::isAdmin()) {
    $import_btn = '';
}
?>

<div class="container journal-filter">
    <?= $this->render('_filter_journal', ['post' => $post]) ?>
</div>
<div class="journal-index">
    <div id="ajaxCrudDatatable">
        <?php try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    ['content' =>
                        Html::a('Создать', ['create'],
                            ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-primary']) .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                        '{toggleData}'
                        .'{export}'
                    ],
                ],
                'exportConfig' => [
                    GridView::EXCEL => [
                        'label' => 'Сохранить в EXCEL',
                        'filename' => 'Журнал_Excel',
                        'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'extension' => 'xlsx',

                    ],
                    GridView::CSV => [
                        'label' => 'Сохранить в CSV',
                        'filename' => 'Журнал_CSV',
                        'colDelemiter' => ';'
                    ],
                ],
                'export' => [
                    'target' => '_self',
                    'showConfirmAlert' => false,
                    'fontAwesome' => true,
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                    'before' => $import_btn,
                    '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), __METHOD__);
            Yii::$app->session->setFlash('error', $e->getMessage());
        } ?>
    </div>

</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
