<?php

/* @var $this yii\web\View */
/* @var $model app\models\Node */
?>
<div class="node-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
