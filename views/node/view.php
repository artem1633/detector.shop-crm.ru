<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Node */
?>
<div class="node-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
