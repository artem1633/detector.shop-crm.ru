<?php

use app\models\Parameter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Arrayhelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\Option;
use app\models\OurObject;
use app\models\Equipment;

?>



<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'hidden-print',
    ],
]); ?>

<div class="row">

    <?php
    $layout = <<< HTML
    <span class="input-group-addon" style="background-color: #ecf0f5; color:black;">С</span>
    {input1}
    <span class="input-group-addon" style="background-color: #ecf0f5; color:black;">По</span>
    {input2}
    <span class="input-group-addon kv-date-remove" style="background-color: #ecf0f5; color:black;">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
    ?>

    <div class="col-md-5 report">
        <label style="margin-left: 15px;">Интервал</label><br>
        <?php
        echo DatePicker::widget([
            'type' => DatePicker::TYPE_RANGE,
            'name' => 'date_time_from',
            'value' => $post['date_time_from'],
            'name2' => 'date_time_to',
            'value2' => $post['date_time_to'],
            'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
            'layout' => $layout,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy',
            ]
        ]);
        ?>
    </div>

    <div class="col-md-5 report">
        <label style="margin-left: 15px;">Параметры</label><br>
        <?php
        echo Select2::widget([
            'name' => 'parametr', 'language' => 'ru',
            'value' => $post['parametr'],
            'data' => Arrayhelper::map(Option::find()->where(['!=', 'value_type_id', 4])->andWhere(['!=', 'value_type_id', 5])->all(), 'id', 'name'),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Выбирите параметры', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-5 report">
        <label style="margin-left: 15px;">Оборудования</label><br>
        <?php
        echo Select2::widget([
            'name' => 'oborudovaniya', 'language' => 'ru',
            'value' => $post['oborudovaniya'],
            'data' => Arrayhelper::map(Equipment::find()->all(), 'id', 'name'),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Выбирите оборудования', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-5 report">
        <label style="margin-left: 15px;">Предпиятие</label><br>

        <?=
        Select2::widget([
            'name' => 'object',
            'language' => 'ru',
            'value' => $post['object'],
            'data' => ArrayHelper::map(OurObject::find()->all(), 'id', 'name'),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Выбирите объект', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
        <?php // Html::dropDownList('object', $post['object'],
        //            ArrayHelper::map(OurObject::find()->all(), 'id', 'name'),
        //            [
        //            'style' => 'width:437px !important; height:35px;border-radius:5px;;',
        //        ]) ?>

    </div>

    <div class="row">
        <div class="col-md-5 report">
            <label style="margin-left: 15px;">Группировка по параметру:</label><br>
            <?=
            Select2::widget([
                'name' => 'group_param', 'language' => 'ru',
                'value' => $post['group_param'],
                'data' => Arrayhelper::map(Option::find()->where(['!=', 'value_type_id', 4])->andWhere(['!=', 'value_type_id', 5])->all(), 'id', 'name'),
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => 'Выбирите параметр', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4" style="margin-left:30px;">
        <div class="form-group">
            <?= Html::submitButton('Поиск', ['style' => ' ', 'class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
    
