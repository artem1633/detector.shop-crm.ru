<?php

use app\models\Product;
use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="box box-default">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
            'action' => 'set-parameter'
        ]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'object_id')->label()->widget(Select2::classname(), [
                    'data' => \app\models\OurObject::getList(),
                    'options' => [
                        //'value' =>  $model->type,
                        'placeholder' => 'Выберите предприятие',
                        'onchange' => '
                            $.post( "/parameter/lists?id=' . '"+$(this).val(), function( data ){
                                $( "select#parameter-product_id" ).html( data);
                            });
                            $.post( "/parameter/lists-equipment?id=' . '"+$(this).val(), function( data ){
                                $( "select#parameter-equipment_id" ).html( data);
                            });
                        '
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'equipment_id')->label()->widget(Select2::classname(), [
                    'data' => $model->getEquipmentList($model->object_id),//\app\models\Equipment::getList(),
                    'options' => [
                        'placeholder' => 'Выберите оборудование',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'product_id')->label()->widget(Select2::classname(), [
                    'data' => Product::getProductList(),
                    'options' => [
                        'placeholder' => 'Выберите продукт',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <?php if(Users::isAdmin()): ?>
            <div class="col-md-6">
                <?= $form->field($model, 'user_id')->label()->widget(Select2::classname(), [
                    'data' => Users::getList(),
                    'options' => [
                        'placeholder' => 'Выберите пользователя ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <?php else: ?>
                <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false);?>
            <?php endif ?>
        </div>
        <?php echo $form->field($model, 'number')->hiddenInput(['value' => $model->number]) ?>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить в документ' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть документ', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-warning']); ?>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
 $(document).ready(function(){
        $(\'.datee\').each(function(){
            $(this).mask(\'00/00/0000\');
        });
        $(\'.timee\').each(function(){
            $(this).mask(\'00:00\');
        });
    });
');
?>

