<?php

use app\models\Option;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>

<div class="box box-default">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'option_id')->widget(Select2::classname(), [
                    'data' => Option::getUnusedOptions($model->number),
                    'options' => [
                        //'value' =>  $model->type,
                        'placeholder' => 'Выберите параметр',
                        'onchange' => '
                            $.post( "/parameter/get-type-value?id=' . '"+$(this).val(), function( data ){
                                //console.log(data);
                                $date_input = $("#type-date-value");
                                $time_input = $("#type-time-value");
                                $text_input = $("#type-text-value");
                                
                                $date_input.hide();
                                $time_input.hide();
                                $text_input.hide();
                                
                                switch(data) {
                                    case "' . Option::TYPE_DATE . '":
                                        //console.log("Date!");
                                        $date_input.show();
                                        break;
                                    case "'. Option::TYPE_TIME .'":
                                        //console.log("Time!");
                                        $time_input.show();
                                        break;
                                    default:
                                        //console.log("Default!");
                                        $text_input.show();
                                        break;
                                }
//                                $( "select#parameter-product_id" ).html( data);
                            });
                        ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]); ?>
            </div>

            <div id="type-time-value" class="col-md-6" style="display: none;">
                <?= $form->field($model, 'time_value')->widget(MaskedInput::className(), [
                    'mask' => '99:99',
                ])->label('Значение');
                ?>
            </div>
            <div id="type-date-value" class="col-md-6" style="display: none;">
                <?= $form->field($model, 'date_value')->widget(DatePicker::className(), [
                    'options' => [
                        'placeholder' => 'Выберите дату...',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ])->label('Значение') ?>
            </div>
            <div id="type-text-value" class="col-md-6" style="display: none;">
                <?= $form->field($model, 'text_value')->textInput()->label('Значение') ?>
            </div>
        </div>


        <?php echo $form->field($model, 'number')->hiddenInput(['value' => $model->number])->label(false) ?>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить в документ' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть документ', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-warning']); ?>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
 $(document).ready(function(){
        $(\'.datee\').each(function(){
            $(this).mask(\'00/00/0000\');
        });
        $(\'.timee\').each(function(){
            $(this).mask(\'00:00\');
        });
    });
');
?>

