<?php

use app\models\Option;
use kartik\grid\GridView;
use app\models\Parameter;
use yii\data\ActiveDataProvider;

$query = Parameter::find()->where(['number' => $number]);

$provider1 = new ActiveDataProvider([
    'query' => $query,
    'sort' => array(
        'defaultOrder' => ['id' => SORT_DESC],
    ),
]);
?>

<?= GridView::widget([
    'id' => 'crud-datatable',
    'dataProvider' => $provider1,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'product_id',
            'content' => function ($data) {
                return $data->product->name;
            }
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'option_id',
            'content' => function ($data) {
                $measure = $data->option->measure;
                if ($measure) {
                    return $data->option->name . ' (' . $measure . ')';
                } else {
                    return $data->option->name;
                }
            }
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'value',
            'value' => function($data) {
                //Если дата, то форматируем значение для вывода
                if ($data->option->value_type_id == Option::TYPE_DATE){
                    return date('d/m/Y', strtotime($data->value));
                } else {
                    return $data->value;
                }
            }
        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
]) ?>
