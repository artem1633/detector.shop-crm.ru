<?php

use app\models\Option;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="box box-default">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'option_id')->textInput([
                    'value' => Option::getName($model->option_id),
                    'disabled' => true,
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?php if ($model->option->value_type_id == Option::TYPE_DATE): ?>
                    <?php
                    $model->value = date('d/m/Y', strtotime($model->value));
                    echo $form->field($model, 'value')->widget(DatePicker::className(), [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'options' => [
                            'placeholder' => 'Введите или выберите дату (дд/мм/гггг)'
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd/mm/yyyy'
                        ]
                    ]) ?>
                <?php elseif ($model->option->value_type_id == Option::TYPE_TIME): ?>
                    <?= $form->field($model, 'value')->widget(\yii\widgets\MaskedInput::className(),[
                            'mask' => '99:99'
                    ]) ?>
                <?php else:?>
                <?= $form->field($model, 'value')->textInput();
                ?>
                <?php endif; ?>
            </div>
        </div>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить в документ' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть документ', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-warning']); ?>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
 $(document).ready(function(){
        $(\'.datee\').each(function(){
            $(this).mask(\'00/00/0000\');
        });
        $(\'.timee\').each(function(){
            $(this).mask(\'00:00\');
        });
    });
');
?>

