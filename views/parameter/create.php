<?php

use app\models\Option;
use app\models\Parameter;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;


CrudAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Available */
$this->title = 'Поступление:';
?>


<?php if ($model->number): ?>
    <div class="available-update">
        <?php
        $doc_model = Parameter::findOne($model->number);
        echo $this->render('_small_form', [
            'model' => $doc_model,
        ]) ?>
    </div>
    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'attribute' => 'option_id',
                'value' => function ($data) {
                    $measure = $data->option->measure;
                    if ($measure) {
                        return $data->option->name . ' (' . $measure . ')';
                    } else {
                        return $data->option->name;
                    }
                },
//                'value' => 'option.name',
            ],
            [
                'attribute' => 'value',
                'value' => function ($data) {
                    $option_id = $data->option_id ?? null;

                    Yii::info('Код типа значения: ' . $data->option->value_type_id, 'test');

                    if ($option_id && $data->option->value_type_id == Option::TYPE_DATE) {
                        return date('d/m/Y', strtotime($data->value));
                    }
                    return $data->value;
                }

            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to([$action, 'id' => $key]);
                },
                'updateOptions' => ['role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?'],
            ],
        ],
        'toolbar' => [
            ['content' =>
                Html::a('Добавить параметр', ['add-parameter', 'number' => $model->number],
                    ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-primary'])
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'type' => 'primary',
            'heading' => '<i class="glyphicon glyphicon-list"></i> Документ',
            'before' => '',
            'after' => '',
        ]
    ]) ?>

    <?php Modal::begin([
        "id" => "ajaxCrudModal",
        "footer" => "",// always need it for jquery plugin
    ]) ?>
    <?php Modal::end(); ?>
<?php else: ?>
    <div class="available-create">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
<?php endif ?>
