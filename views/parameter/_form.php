<?php

use app\models\Option;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="box box-default">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'object_id')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => \app\models\OurObject::getList(),
                    'options' => [
                        //'value' =>  $model->type,
                        'placeholder' => 'Выберите предприятие',
                        'onchange' => '
                            $.post( "/parameter/lists?id=' . '"+$(this).val(), function( data ){
                                $( "select#parameter-product_id" ).html( data);
                            });
                            $.post( "/parameter/lists-equipment?id=' . '"+$(this).val(), function( data ){
                                $( "select#parameter-equipment_id" ).html( data);
                            });
                        '
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'equipment_id')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getEquipmentList($model->object_id),//\app\models\Equipment::getList(),
                    'options' => [
                        'placeholder' => 'Выберите оборудование',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'product_id')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getProductList($model->object_id),//\app\models\Equipment::getList(),
                    'options' => [
                        'placeholder' => 'Выберите продукт',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

        </div>
        <div class="row">
            <?php $params = Option::find()->all(); ?>
            <h3 style="margin-left: 30px">Параметры</h3>

            <?php foreach ($params as $param): ?>
                <?php Yii::info($param->name, 'test') ?>
                <?php $d = 'form-control'; ?>
                <?php if ($param->value_type_id == Option::TYPE_DATE) {
                    $d .= ' datee';
                }
                if ($param->value_type_id == Option::TYPE_TIME) {
                    $d .= ' timee';

                }
                if ($param->measure) {
                    $param_name = $param->name . ' (' . $param->measure . ')';
                } else {
                    $param_name = $param->name;
                }
                ?>

                <div class="col-md-12 form-group tanla">
                    <div class="col-md-6">
                        <input type='text' class="form-control" value="<?= $param_name ?>" maxlength="255" disabled>
                    </div>
                    <div class="col-md-6">
                        <?php if ($param->value_type_id == Option::TYPE_DATE): ?>
                            <?= DatePicker::widget([
                                'name' => "Paramaa-" . $param->id,
//                                'type' => DatePicker::TYPE_INPUT,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'options' => [
                                    'placeholder' => 'Введите или выберите дату (дд/мм/гггг)'
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd/mm/yyyy'
                                ]
                            ]) ?>
                        <?php else: ?>
                            <input type='text' class="<?= $d; ?>" name="<?= "Paramaa-" . $param->id ?>"
                                   placeholder="<?= Option::getList()[$param->value_type_id] ?>">
                        <?php endif; ?>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить в документ' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть документ', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-warning']); ?>
            </div>
        <?php } ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
 $(document).ready(function(){
        $(\'.datee\').each(function(){
            $(this).mask(\'00/00/0000\');
        });
        $(\'.timee\').each(function(){
            $(this).mask(\'00:00\');
        });
    });
');
?>

