<?php

use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'object_id',
        'content' => function (\app\models\Parameter $parameter) {
            return $parameter->object->name;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'user_id',
//        'content'=>function($data){
//            return $data->getUserNameByID($data->user_id);
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'equipment_id',
        'content' => function (\app\models\Parameter $parameter) {
            return $parameter->equipment->name;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'date',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'option_id',
        'content' => function (\app\models\Parameter $parameter) {
            return $parameter->option->name;
        }
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'value',
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   