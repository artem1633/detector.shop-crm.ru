<?php

use app\models\Option;
use kartik\export\ExportMenu;
use yii\data\ArrayDataProvider;

$this->title = 'Поступившие заявки';
$this->params['breadcrumbs'][] = $this->title;
//Пример дата провайдера для експорта таблицы
$dp1 = ['allModels' => [
    ['id' => 1, 'fruit' => 'Apples', 'quantity' => '100'],
    ['id' => 2, 'fruit' => 'Oranges', 'quantity' => '60'],
    ['id' => 3, 'fruit' => 'Bananas', 'quantity' => '160'],
    ['id' => 4, 'fruit' => 'Pineapples', 'quantity' => '90'],
    ['id' => 5, 'fruit' => 'Grapes', 'quantity' => '290'],
]];
?>

<div class="box box-default ">
    <div class="box-header with-border">
        <h3 class="box-title" data-widget="collapse">Поиск</h3>
    </div>
    <div class="box-body">
        <?php echo $this->render('search', ['model' => $searchModel, 'post' => $post]); ?>
    </div>
</div>

<div class="box box-info box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-cubes"></i> Отчёт</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">

                <i class="fa fa-minus"></i>
            </button>
            <?php //echo Html::a('<i class="glyphicon glyphicon-download"></i>', ['export'],
            //                ['role' => 'modal-remote', 'title' => 'Скачать как файл Excel', 'class' => 'btn btn-default btn-flat']); ?>
        </div>
    </div>

    <div class="box-body">
        <div class="box-body">
            <table class="table table-bordered table-condensed forReport">
                <tr>
                    <th class="forColumn"><b>№</b></th>
                    <th class="forColumn"><b>Параметр</b></th>
                    <th class="forColumn"><b>Минимальные значения</b></th>
                    <th class="forColumn"><b>Средние значения</b></th>
                    <th class="forColumn"><b>Максимальные значения</b></th>
                </tr>

                <?php
                $i = 1;
                $options = Option::find()->where(['!=', 'value_type_id', 4])->andWhere(['!=', 'value_type_id', 5])->all();

                if ($models != null)
                    foreach ($options as $valueOfOption) {
                        Yii::info($valueOfOption->name, 'test');
                        $max = 0;
                        $min = 0;

                        $check = false;

                        $avarage = 0;
                        $summa = 0;
                        $count = 0;

                        foreach ($models as $value) {
                            if ($valueOfOption->id == $value->option_id) {
                                if ($check == false) {
                                    $max = $value->value;
                                    $min = $value->value;
                                    $check = true;
                                }
                                if ($min > $value->value) $min = $value->value;
                                if ($max < $value->value) $max = $value->value;
                                $count++;
                                $summa += $value->value;
                            }
                        }
                        if ($check) {
                            ?>
                            <tr>
                                <td class="forColumn"><?= $i; ?></td>
                                <td class="forColumn"><?= $valueOfOption->name; ?></td>
                                <td class="forColumn"><?= $min; ?></td>
                                <td class="forColumn"><?= $summa / $count; ?></td>
                                <td class="forColumn"><?= $max; ?></td>
                            </tr>
                            <?php
                            //Для експорта
                            $dp['allModels'] = [
                                ['num' => $i, 'param_name' => $valueOfOption->name, 'min' => $min, 'med' => $summa / $count, 'max' => $max]
                            ];
                            ?>
                            <?php
                            $i++;
                        }
                    }
                $dataProvider = new ArrayDataProvider($dp);
                ?>


            </table>
            <?php
            if ($dp) {
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
//                    'columns' => ['id', 'fruit', 'quantity'],
                    'options' => ['id' => 'expMenu1'], // optional to set but must be unique
                    'target' => ExportMenu::TARGET_BLANK,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_CSV  => false,
                        ExportMenu::FORMAT_TEXT  => false,
                        ExportMenu::FORMAT_PDF  => false,
                    ]
                ]);
            }
            ?>
        </div>
    </div>

</div>

