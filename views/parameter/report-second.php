<?php

use app\models\Option;
use kartik\export\ExportMenu;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\grid\GridView;

/**
 * @param mixed $models модели app/models/Parameter;
 * @param mixed $param_query Запрос ActiveQuery с основными параметрами (без option_id)
 * @param mixed $models Выборка моделей по всем основным параметрам
 * @param array $post Пост массив с набором данных из фильтров
 * @param array $unique_values Уникальные значения параметра выбранного в фильтре "Уникальные значения по параметру":
 */

$this->title = 'Поступившие заявки';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-default ">
    <div class="box-header with-border">
        <h3 class="box-title" data-widget="collapse">Поиск</h3>
    </div>
    <div class="box-body">
        <?php echo $this->render('search-second', ['model' => $searchModel, 'post' => $post]); ?>
    </div>
</div>
<div>

    <?php
    $all_models = [];

    $i = 1;
    $counter = 0;

    if ($models) {
        //Получаем все строки по выбранным параметрам
        $target_options = Option::find()->where(['id' => $post['group_param']])->all();
        $unique_parameter = Option::getName($post['unique_param']);

        //Проходимся по поулченным параметрам
        foreach ($target_options as $option) {
            $query = clone $param_query;
            $result = $query
                ->andWhere(['option_id' => $option->id])
                ->all();
            $min = 0;
            $max = 0;

            $check = false;

            $avarage = 0;
            $summa = 0;
            $count = 0;

            foreach ($result as $value) {
                if ($value->value) {
                    if ($check == false) {
                        $max = $value->value;
                        $min = $value->value;
                        $check = true;
                    }
                    if ($min > $value->value) $min = $value->value;
                    if ($max < $value->value) $max = $value->value;
                    $count++;
                    $summa += $value->value;
                }
            }

            if ($count) {
                $med = round($summa / $count, 3);
            } else {
                $med = 0;
            }
            //Добавляем "строку" в отчет (Параметр группировки)
            array_push($all_models,
                [
                    'num' => $i,
                    'param_name' => $option->name,
                    'min' => '&nbsp;' . $min, //Пробел обязателен, т.к. без него в Excele некоторые значения отображаются как даты
                    'med' => '&nbsp;' . $med,
                    'max' => '&nbsp;' . $max,
                ]
            );
            $counter++;
            $j = 1;
            foreach ($unique_values as $u_value) {
                if ($u_value->value) {
                    $query_1 = clone $param_query;

                    $result_1 = $query_1
                        ->andWhere(['value' => $u_value->value])
                        ->all();
                    $min = 0;
                    $max = 0;

                    $check = false;

                    $avarage = 0;
                    $summa = 0;
                    $count = 0;

                    foreach ($result_1 as $r_value) {
                        $query_2 = clone $param_query;
                        $result_2 = $query_2
                            ->andWhere(['number' => $r_value->number])
                            ->andWhere(['option_id' => $option->id])
                            ->all();
                        foreach ($result_2 as $r2_value) {
                            if ($r2_value->value) {
                                if ($check == false) {
                                    $max = $r2_value->value;
                                    $min = $r2_value->value;
                                    $check = true;
                                }
                                if ($min > $r2_value->value) $min = $r2_value->value;
                                if ($max < $r2_value->value) $max = $r2_value->value;
                                $count++;
                                $summa += $r2_value->value;
                            }
                        }

                    }
                    if ($count) {
                        $med = round($summa / $count, 3);
                    } else {
                        $med = 0;
                    }
                    // Добавляем "строку" в отчет
                    array_push($all_models,
                        [
                            'num' => '&nbsp;' . $i . '.' . $j,//Пробел обязателен, т.к. без него в Excele некоторые значения отображаются как даты
                            'param_name' => $unique_parameter . ' ' . $u_value->value,
                            'min' => '&nbsp;' . $min,
                            'med' => '&nbsp;' . $med,
                            'max' => '&nbsp;' . $max,
                        ]
                    );
                }
                $j++;
            }
            $i++;
        }
    }

    $dataProvider = new ArrayDataProvider(['allModels' => $all_models]);
    $dataProvider->pagination = false;

    //    $dataProvider = new \yii\data\ActiveDataProvider([
    //            'query' => \app\models\Parameter::find()->where(['>', 'option_id', 7 ]),
    //    ]);

//    $test_models = [];
//    foreach ($all_models as $model) {
//        array_push($test_models, $model);
//    }

//    Yii::info($test_models, 'test');

//    $testProvider = new ArrayDataProvider([
//        'allModels' => $test_models,
//        'pagination' => [
//            'pageSize' => 10,
//        ],
//        'sort' => [
//            'attributes' => ['param_name', 'min'],
//        ]
//    ]);

    $grid_columns = [
        [
            'attribute' => 'num',
            'label' => '#',
            'value' => function ($data) {
                if (isset($data['num'])) {
                    $n_data = $data['num'];
                    if (!strpos($n_data, '.')) {
                        return Html::tag('p', $n_data, ['style' => 'font-weight: bold;']);
                    } else {
                        return Html::tag('p', $n_data, ['style' => 'padding-left: 1rem;']);

                    }
                }
                return null;
            },
            'format' => 'raw',
            'vAlign' => 'middle',
            'contentOptions' => ['cellFormat' => DataType::TYPE_STRING]
        ],
        [
            'attribute' => 'param_name',
            'label' => 'Наименование параметра',
            'value' => function ($data) {
                if (isset($data['param_name'])) {
                    $n_data = $data['param_name'];
                    if (!strpos($data['num'], '.')) {
                        return Html::tag('p', $n_data, ['style' => 'font-weight: bold;']);
                    } else {
                        return Html::tag('p', $n_data, ['style' => 'padding-left: 1rem;']);

                    }
                }
                return null;
            },
            'format' => 'raw',
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'min',
            'label' => 'Мин.',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'format' => 'raw',
        ],
        [
            'attribute' => 'med',
            'label' => 'Сред.',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'format' => 'raw',
        ],
        [
            'attribute' => 'max',
            'label' => 'Макс.',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'format' => 'raw',
        ],

    ];

    //    $grid_columns = [
    //            'option_id',
    //            'value',
    //    ]
//
//    $testProvider = new \yii\data\ArrayDataProvider(['allModels' =>
//        [
//            [
//                'num' => 1,
//                'param_name' => 'Время после старта',
//                'min' => '&nbsp;0.041666666666667',
//                'med' => '&nbsp;0.245',
//                'max' => '&nbsp;0.47916666666667',
//            ],
//            [
//                'num' => '&nbsp;1.2',
//                'param_name' => 'Плотность пульпы 1750',
//                'min' => '&nbsp;0.125',
//                'med' => '&nbsp;0.125',
//                'max' => '&nbsp;0.125',
//            ]
//        ]
//    ]);

    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            $export_menu = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $grid_columns,
                'dropdownOptions' => [
                    'label' => 'Экспортировать результат',
                    'class' => 'btn btn-default'
                ],
                'showConfirmAlert' => true,
                'showColumnSelector' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_CSV => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_PDF => false,
                    ExportMenu::FORMAT_EXCEL => false,
                ]
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => $grid_columns,
                'toolbar' => [
                    ['content' =>
                        '{export}'
//                        . $export_menu

                    ],
                ],
                'exportConfig' => [
                    GridView::EXCEL => [
                        'label' => 'Сохранить в EXCEL',
                        'filename' => 'Отчет',
                        'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'extension' => 'xlsx',

                    ],
                    GridView::CSV => [
                        'label' => 'Сохранить в CSV',
                        'filename' => 'CSV Отчет',
                        'colDelemiter' => ';'
                    ],
                ],
                'export' => [
                    'target' => '_self',
                    'showConfirmAlert' => false,
                    'fontAwesome' => true,
                ],

                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Результат построения отчета',
                    'after' => '<div class="clearfix"></div>',
                ],
            ]);

            ?>
        </div>
    </div>


</div>


