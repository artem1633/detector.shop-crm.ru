<?php

use yii\widgets\DetailView;

?>
<div class="equipment-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
</div>
