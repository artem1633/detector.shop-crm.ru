<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OurObject */
$this->title = 'Просмотр';
?>
<div class="box box-solid box-primary">    
    <div class="box-header">
        <h3 class="box-title">Объект</h3>
        <span class="pull-right">
        	<?=Html::a('Назад', ['index'], ['data-pjax'=>'0', 'title'=> 'Назад', 'class'=>'btn btn-warning btn-xs'])?>
        </span>
    </div>
    <div class="box box-default">   
        <div class="box-body">
		    <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
		            'id',
		            'name',
		        ],
		    ]) ?>
		</div>
	</div>
</div>
