<?php

use yii\widgets\DetailView;

?>
<div class="product-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
</div>
