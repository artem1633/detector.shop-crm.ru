<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Element */
?>
<div class="element-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'label' => 'Предприятие',
                'value' => function (\app\models\Element $element) {
                    return $element->object->name;
                },
            ],
            //'object_id',
            //'equipment_id',
            [
                'label' => 'Оборудование',
                'value' => function (\app\models\Element $element) {
                    return $element->equipment->name;
                },
            ],
            'element_number',
            'element_row',
            'number',
            'modification',
            'date_ust',
            'date_dem',
            'dem_reason',
            'comment',
            //'user_id',
            [
                'label' => 'Пользователь',
                'value' => function (\app\models\Element $element) {
                    return $element->user->fio;
                },
            ],
        ],
    ]) ?>

</div>
